//Tabs

let fActive = '';

function filterTabs(theme){
    if(fActive !== theme){
        $('.all').filter('.'+theme).fadeIn();
        $('.all').filter(':not(.'+theme+')').fadeOut();
        fActive = theme;
    }
}

$('.all-options').click(function(){ filterTabs('all'); });
$('.monosort-options').click(function(){ filterTabs('mono'); });
$('.firmovi-options').click(function(){ filterTabs('firm'); });
$('.robusta-options').click(function(){ filterTabs('robusta'); });